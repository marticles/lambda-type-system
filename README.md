# Kodowanie algebry boola w lambda calculus w turing complete systemie typów

## Czym jest algebra boola?

Algebra boola to algebra zawierająca tylko dwie wartości: `true` (prawda) i
`false` (fałsz), zamiast liczb (jak to mamy w standardowej algebrze). Dodatkowo
różnią się operacje. W algebrze boola używamy operacji jak `not`, `and`, `or`
itp. zamiast `+`, `-`, `*`, `/` itd.

Nie wiem do czego jest używana poza programowaniem, ale w programowaniu jest ona
jedną z pierwszych rzeczy, których trzeba się nauczyć. Używa się jej do łączenia
logicznych stwierdzeń. Na przykład, jeśli chcemy zrobić transfer pieniędzy
musimy upewnić się, że:

- Użytkownik ma wystarczająco pieniędzy
- Konto docelowe istnieje
- Masa innych rzeczy, o których nie wiem, lub nie są nam potrzebne w tym
  przykładzie

W Rust moglibyśmy zapisać to w ten sposób:

```rust
if user.funds >= amount && destination_account.exists() && moreThingsThatIDontCareAbout() {
    makeTransfer();
} else {
    showError();
}
```

W tym przykładzie sprawdzamy czy użytkownik ma wystarczająco pieniędzy
(`user.funds >= amount`). Wyrażenie to wykonuje się do wartości `true` lub
`false`, czyli wartości z algebry boola. Sprawdzamy czy konto docelowe
istnieje (`destination_account.exists()`). To wyrażenie również wykonuję się do
wartości `true` lub `false`. Wykonujemy operację `&&` (`and`, i) na
uzyskanych wartościach, której wynikiem będzie `true`, jeżeli obie wartości są
`true`, a `false` w każdym innym przypadku (`true && false`, `false && true`,
`false && false`). Robimy to samo z uzyskaną wartością i wynikiem wywołania
funkcji `moreThingsThatIDontCareAbount()`. W zależności od rezultatu całego tego
wyrażenia instrukcja `if` (która w Rust jest również wyrażeniem) wykonuje
gałąź z wywołaniem funkcji `makeTransfer`, jeżeli `true` lub `showError`, jeżeli
`false`.

I w ten oto sposób możemy wykorzystać algebre boola w programowaniu. Bez niej
programowanie byłoby czymś zupełnie innym niż obecnie.

## Czym jest lambda calculus?

Lambda calculus to model komputacji, który może być alternatywą do maszyny
Turinga. Co ciekawe, została ona stworzona przez doktoranta Alana Turinga -
Alonzo Church.

Składnia lambda calculus składa się z trzech rzeczy:

- Zmiennych `x`
- Abstrakcji `λ`
- Aplikacji `(λx.x)y`

Te trzy proste rzeczy wystarczą aby zakodować każdą możliwą komputację. Jako
przykład zakodujemy agebre boola (to jest bardzo popularny przykład). Na
początku w czystym lambda calculus (niewielką częśś), a potem w systemie typów
języka Rust (tą samą część + extra).

Jeśli nie masz jeszcze żadnego doświadczenia z lambda calculus, proponuję
przeczytać ten rozdział dokładnie, ponieważ system typów Rust jest mniej
czytelny i możesz mieć później problemy z jego zrozumieniem.

Zaczniemy od implementacji `TRUE` i `FALSE`:

```
TRUE = λxy.x
FALSE = λxy.y
```

Zanim wytłumaczę te linie, muszę Cię poinformować, że użyłem tu pewnego skrótu.
Lamdbdy mogą przyjmować tylko jeden argument. Lambdy, które biorą więcej niż
jeden są w rzeczywistości lambdami zwracającymi lambdy: `λx.λy.y`. Aby uczynić
rzeczy prostsze do napisania (jak i przeczytania), będę używał tego skóconego
zapisu. O, i dodawanie (znak `+`) nie jest częścią lambda calculus, ale może
zostać zakodowane jak wszystko inne.

Teraz wytłumaczenie. Linie na górze są definicją dwóch rzeczy. Pierszą z nich
jest `TRUE`, która jest lambdą przyjmującą `x` i `y` i zwracającą `x`. Druga
to `FALSE`. Prawie to samo co `TRUE`, ale zwraca `y` zamiast `x`.

Przykładowe wywołania naszych lambd mogą wyglądać jak poniżej:

```
TRUE FALSE TRUE = FALSE
FALSE TRUE FALSE = TRUE
```

Nie będę teraz tłumaczył dlaczego są zdefiniowane w ten sposób, ale
najprawdopodobniej zrozumiesz, kiedy przyjżysz się definicji `NOT`:

```
NOT = λx.x FALSE TRUE
```

`NOT` przyjmuje pojedyńczy argument `x` i aplikuje go z argumentami `FALSE` i
`TRUE`. Spróbuj przemyśleć jaki będzie wynik wywołania tej definicji najpierw z
`TRUE`, a potem z `FALSE` jako argument (w następnym paragrafie wytłumaczenie).

Jeśli do `x` zostanie przypisana wartość `TRUE`, wtedy `NOT` wykona to `TRUE`
(`λxy.x`) z `FALSE` i `TRUE` jako argumenty. A jak wiemy z definicji `TRUE`
zwraca ono swój pierwszy argument, czyli `FALSE`. I dokładnie tego oczekujemy
od `NOT` (`NOT TRUE = FALSE`). Tak samo działa to w przypadku `FALSE`.

Znasz już podstawy lambda calculus. Możesz spróbować zdefiniować samodzielnie
`AND` lub `OR`. Nie martw się jeśli nie dasz rady, zrobimy to w następnym
rozdziale (ale w systemie typów), gdzie wszystko wytłumaczę.

## Lambda calculus w systemie typów języka Rust

Mogłeś kiedyś słyszeć, że istnieją języki programowania z turing complete
systemem typów. Przykładami takich języków jest Rust, TypeScript, Haskell, Scala
i wiele, wiele więcej. Oznacza to w skrócie, że w samym systemie typów tych
języków możemy zakodować każdą możliwą komputację, która zostanie wykonana w
czasie kompilacji, a nie przy uruchomieniu programu.

Wykorzystamy to do zakodowania części agebry boola w systemie typów języka Rust.
Ale czy to jest w ogóle przydatne w rzeczywistym programowaniu? Ta konkretna
rzecz - nie, ale pośrednio - może. Dziś robię to w ramach zabawnego eksperymentu
oraz jako niewielkie wyzwanie. Ale systemy typów to bardzo potężna rzecz.
Pomagają nam przy sprawdzaniu poprawności programu, sprawiają, że kod jest
prostszy do zrozumienia (w Haskellu często wystarczy przeczytać sam typ funkcji,
aby zrozumieć w pełni jej działanie). A wszystko to sprawia, że programista
pisze raczej lepszy kod. Ale działa to trochę jak młotek, pomaga Ci zrobić co
masz do zrobienia, ale możesz sobie stłóc palec (na przykład używając poniższego
kodu w rzeczywistym programie, tylko dlatego, że tak się da). Podsumowując -
tak może to być przydatne.

Koniec gadania. Zaczynajmy! Na początek typ, który reprezentuje lambde:

```rust
trait Lambda<T> {
  type Output;
}
```

Lambda przyjmuje pojedyńczy argument `T` i zwraca jakąś wartość `Output`. Prosta
sprawa. Teraz `True` i `False`:

```rust
struct True;

impl True {
  const VALUE: bool = true;
}

impl<A, B> Lambda<(A, B)> for True {
  type Output = A;
}

struct False;

impl False {
  const VALUE: bool = false;
}

impl<A, B> Lambda<(A, B)> for False {
  type Output = B;
}
```

Ten kod zawiera dwie definicje typu `True` i `False`. Implementacje ze stałą
`VALUE` pozwolą nam wyświetlić wynik naszych obliczeń makrem `println!`, a
będzie to przydatne, bo miłobyłoby wiedzieć, że to co stworzyliśmy działa.

Oba typy implementują również trait `Lambda`. Ich `T` jest tuple zawierający
wartości dwóch generycznych argumentów `A` i `B`. Wartości `TRUE` i `FALSE` z
dwoma argumentami brzmią znajomo ;) `Output` dla implementacji `True` zwraca
pierwszy argument (`A`), natomiast `False` zwraca drugi (`B`). Dokładnie tak
samo jak w definicji w czystym lambda calculus.

Przykładowe wykonanie tych definicji:

```rust
fn main() {
    println!(
        "true false true = {}",
        <True as Lambda<(False, True)>>::Output::VALUE
    );
    println!(
        "false false true = {}",
        <True as Lambda<(False, True)>>::Output::VALUE
    );
}
```

Nie zapominaj, że to jest prawdziwy, działający kod. Spróbuj wpisać go w swoim
edytorze i uruchomić!

Następnie definicja lambdy `Not`:

```rust
struct Not;

impl<T: Lambda<(False, True)>> Lambda<T> for Not {
    type Output = T::Output;
}
```

Implementacja `Lambda` dla `Not` przyjmuje jeden argument `T`. Musi on być innym
typem implementującym `Lambda<(False, True)>`, czyli lambdą przyjmującą argument
`False` i `True`. Zerknij teraz proszę jeszcze raz na definicję `Lambda` dla
`True` oraz `False`. Oba te typy są lambdami przyjmującymi dwa dowolne argumenty
czyli między innymi `False` i `True`. Oznacza to, że oba pasują do argumentu `T`
implementacji `Lambda` dla typu `Not`.

Spróbujmy to wykonać:

```rust
fn main() {
    // ...

    println!("!false = {}", <Not as Lambda<False>>::Output::VALUE);
    println!("!true = {}", <Not as Lambda<True>>::Output::VALUE);
}
```

Wow! To działa i nie jest aż tak skomplikowane! Spróbujmy z czymś cięższym - typ
`And`:

```rust
struct And;

impl<A: Lambda<(B, False)>, B> Lambda<(A, B)> for And {
    type Output = <A as Lambda<(B, False)>>::Output;
}
```

Implementacja `Lambda` dla typu `And` przyjmuje dwa argumenty `A` i `B`. `B`
może być czymkolwiek. W naszym wypadku tym "czymkolwiek" jest typ `True` lub
`False`. `A` musi być lambdą, która przyjmuje `B` jako pierwszy argument i
`False` jako drugi. A `Output` to wynik wywołania `A` z pasującymi argumentami.

Ok, pomyślmy co się stanie jeśli `A` będzie równe `True`, a `B` będzie równe
`False`. W tym przypadku `A` zwraca swój pierwszy argument, czyli `B`, które
jest `False`, więc wynikiem wywoływania naszego `And` będzie `False`.

A co jeśli do `A` przypiszemy `False`, a do `B` cokolwiek, ponieważ czysto
teoretycznie, w tej sytuacji wartość `B` nie powinna mieć znaczenia. Wywołujemy
`A`, które jest `False` z argumentami `B` i `False`, jako że `False` (`A`)
zwraca swój drugi argument (w tym przypadku `False`) wynikiem całego wyrażenia
jest `False`. Super! Dokładnie o to nam chodziło.

Sprawdźmy czy to działa w kodzie:

```rust
fn main() {
    // ...

    println!(
        "false && false = {}",
        <And as Lambda<(False, False)>>::Output::VALUE
    );
    println!(
        "true && false = {}",
        <And as Lambda<(True, False)>>::Output::VALUE
    );
    println!(
        "false && true = {}",
        <And as Lambda<(False, True)>>::Output::VALUE
    );
    println!(
        "true && true = {}",
        <And as Lambda<(True, True)>>::Output::VALUE
    );
}
```

Świetnie! Wszystko działa jak należy i to w czasie kompilacji.

## Podsumowanie

To wszystko w tym wpisie. Proponuję zaimplementować samemu typ `Or` w
jakimkolwiek języku z turing complete systemem typów. Jeśli potrzebujesz pomocy
możesz sprawdzić cały kod w [repozytorium na GitLabie](https://gitlab.com/marticles/lambda-type-system).
Znajdziesz tam implementacje wszystkiego co tu napisałem oraz dodatkowo typu
`Or`. Do zobaczenia w kolejnym artykule!
